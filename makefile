extract_lzo_kern_version: %: %.c
	$(CC) -Wall -Wextra -Wpedantic $@.c -o $@ -llzo2

.PHONY: clean
clean:
	$(RM) extract_lzo_kern_version
