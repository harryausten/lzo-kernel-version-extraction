// SPDX-License-Identifier: MIT
#include <ctype.h>
#include <endian.h>
#include <fcntl.h>
#include <lzo/lzo1x.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

// header flags (32-bit unsigned integers)
#define F_ADLER32_D 0x00000001U
#define F_ADLER32_C 0x00000002U
#define F_CRC32_D   0x00000100U
#define F_CRC32_C   0x00000200U

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

static uint8_t *bytes;
static size_t size;
static struct lzop_header head;

struct lzop_header {
	uint16_t version_be16;
	uint16_t lib_version_be16;
	uint16_t version_needed_to_extract_be16;
	uint8_t  method;
	uint8_t	 level;
	// be32 on disk, but we keep this field in native order
	uint32_t flags32;
	uint32_t mode_be32;
	uint32_t mtime_be32;
	uint32_t gmtdiff_be32;
	uint8_t  name_len;
	// name may exist here, but we don't care so just skip over it
	// char     name[255+1];
} __attribute__((packed));

static const uint8_t magic[] = { 0x89, 0x4C, 0x5A, 0x4F, 0x00, 0x0D, 0x0A, 0x1A, 0x0A };

static inline uint32_t read_word(size_t *off)
{
	const uint32_t word = be32toh(*(uint32_t *)(&bytes[*off]));

	*off += 4;
	return word;
}

static const char ver_string[] = "Linux version ";
#define VER_LEN (sizeof(ver_string) - 1)

static bool linux_version(uint8_t *buf, size_t len)
{
	for (size_t pos = 0; pos + VER_LEN < len; ++pos)
		if (memcmp(ver_string, &buf[pos], VER_LEN) == 0) {
			size_t beg = pos + VER_LEN;
			size_t end = beg;

			for (pos = beg; pos < len && isgraph(buf[pos]); ++pos, ++end);
			printf("%.*s\n", (int)(end - beg), &buf[beg]);
			return true;
		}
	return false;
}

static bool decompress(size_t off)
{
	while (true) {
		uint32_t dst_len;
		uint32_t src_len;
		size_t written;
		uint8_t *buf;
		bool found;
		int ret;

		if (off + 8 > size) {
			fputs("Corruption!\n", stderr);
			return false;
		}

		dst_len = read_word(&off);
		// 0 => final block
		if (!dst_len)
			break;

		src_len = read_word(&off);
		if (src_len > dst_len) {
			fputs("File corrupted!\n", stderr);
			return false;
		}

		// skip past uncompressed block checksum
		if ((head.flags32 & F_ADLER32_D) || (head.flags32 & F_CRC32_D))
			off += 4;
		// skip past compressed block checksum
		if ((head.flags32 & F_ADLER32_C) || (head.flags32 & F_CRC32_C))
			off += 4;

		if (off + src_len > size) {
			fputs("Corruption!\n", stderr);
			return false;
		}

		buf = malloc(dst_len);
		if (!buf) {
			perror("malloc");
			return false;
		}

		written = dst_len;
		ret = lzo1x_decompress_safe(&bytes[off], src_len, buf, &written, NULL);
		if (ret != LZO_E_OK || written != dst_len) {
			fputs("LZO decompress failed!\n", stderr);
			free(buf);
			return false;
		}

		found = linux_version(buf, dst_len);
		free(buf);
		if (found)
			return true;
		off += src_len;
	}

	return true;
}

static size_t read_header(size_t off)
{
	memcpy(&head, &bytes[off], sizeof(head));
	head.flags32 = be32toh(head.flags32);

	/* Uncomment for debug
	printf("version = %u\n", be16toh(head.version_be16));
	printf("lib_version = %u\n", be16toh(head.lib_version_be16));
	printf("version_needed_to_extract = %u\n", be16toh(head.version_needed_to_extract_be16));
	printf("method = %u\n", head.method);
	printf("level = %u\n", head.level);
	printf("flags = 0x%08X\n", head.flags32);
	printf("name_len = %u\n", head.name_len);
	*/

	// proceed past header, name and checksum
	off += sizeof(head) + head.name_len + 4;

	return off;
}

static bool lzop_magic(size_t off)
{
	return (memcmp(&bytes[off], magic, ARRAY_SIZE(magic)) == 0);
}

int main(int argc, char *argv[])
{
	struct stat st;
	int fd;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s file\n", argv[0]);
		return 1;
	}

	fd = open(argv[1], O_RDONLY);
	if (fd == -1) {
		perror("open");
		return 1;
	}

	if (fstat(fd, &st)) {
		perror("fstat");
		goto err_close_file;
	}

	if (st.st_size <= 0) {
		fprintf(stderr, "Invalid file size!\n");
		goto err_close_file;
	}

	size = st.st_size;
	bytes = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
	if (bytes == MAP_FAILED) {
		perror("mmap");
		goto err_close_file;
	}

	if (close(fd)) {
		perror("close");
		return 1;
	}

	for (size_t off = 0; off + ARRAY_SIZE(magic) + sizeof(struct lzop_header) < size;) {
		if (lzop_magic(off)) {
			size_t end_of_header;

			off += ARRAY_SIZE(magic);
			end_of_header = read_header(off);
			if (head.method != 3) {
				printf("method = %u unknown!\n", head.method);
				continue;
			}
			if (decompress(end_of_header))
				break;
		} else
			off++;
	}

	if (munmap(bytes, size)) {
		perror("munmap");
		return 1;
	}

	return 0;

err_close_file:
	if (close(fd))
		perror("close");

	return 1;
}
